export const CHART_OPTNS = {
  scaleOverlay: true,
  responsive: true,
  pointHitDetectionRadius: 2,
  maintainAspectRatio: false,
  scaleShowVerticalLines: false,
  scaleBeginAtZero: true,
  barStrokeWidth: 1,
  barValueSpacing: 2,
  scaleGridLineColor: '#DFDFDF',
  scaleLineColor: 'transparent',
};

export const CHART_COLORS = {
  fillColor: 'rgba(31,120,209,0.1)',
  strokeColor: 'rgba(31,120,209,1)',
  highlightFill: 'rgba(31,120,209,0.3)',
};
